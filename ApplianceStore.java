import java.util.Scanner;

public class ApplianceStore {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in); 

        Refrigerator[] appliances = new Refrigerator[4]; 

        for(int i = 0; i<4; i++){
            appliances[i] = new Refrigerator();
        }

        System.out.println(appliances[3].food + " before");
        System.out.println(appliances[3].drinks + " before");
        System.out.println(appliances[3].ice + " before");
        
        appliances[3].food = setFood();
        appliances[3].drinks = setDrinks();
        appliances[3].ice = setIce();

        System.out.println(appliances[3].food + " after");
        System.out.println(appliances[3].drinks + " after");
        System.out.println(appliances[3].ice + " after");

        Refrigerator constructor = new Refrigerator(scan.nextLine(), Integer.parseInt(scan.nextLine()), Double.parseDouble(scan.nextLine()));
    
        
    }
    public static String setFood(){
        Scanner scan = new Scanner(System.in); 

        String food = scan.nextLine();
        return food;
    }
    public static int setDrinks(){
        Scanner scan = new Scanner(System.in); 

        int drinks = scan.nextInt();
        return drinks;
    }
    public static double setIce(){
        Scanner scan = new Scanner(System.in); 

        double ice = scan.nextDouble();    
        return ice;    
    }
}

